﻿#include <iostream>
#include <math.h>

//1.Создать класс со своими данными, скрытыми при помощи private.Сделать public метод для вывода этих данных.Протестировать.
class Var
{
public:
    Var() : First_Var(0), Second_Var(0)
    {}
    void Set_Var(double _First_Var, double _Second_Var)
    {
        First_Var = _First_Var;
        Second_Var = _Second_Var;
    }
    void Show_Var()
    {
        std::cout << "\n" << "First_var = " << First_Var;
        std::cout << "\n" << "Second_var = " << Second_Var;
    }
    double Sum_Var()
    {
        return First_Var + Second_Var;
    }

private:
    double First_Var;
    double Second_Var;
};


//2. Дополнить класс Vector public методом, который будет возвращать длину (модуль) вектора. Протестировать. 
class Vector
{
public:
    Vector() : x(0), y(0), z(0)
    {}
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}
    void Show()
    {
        std::cout << "\n" << x << ' ' << y << ' ' << z;
    }

    double Length() // Модуль вектора
    {
        return sqrt(x * x + y * y + z * z);
    }

private:
    double x;
    double y;
    double z;
};

int main()
{
    Vector v(10.0, 10.0, 10.0);
    v.Show();
    std::cout << "\n" << "Vector length: " << v.Length(); // Вывод на экран модуля вектора

    Var One;
    One.Show_Var();
    std::cout << "\n" << "Sum Var: " << One.Sum_Var();


    One.Set_Var(10, 12);
    One.Show_Var();
    std::cout << "\n" << "Sum Var: " << One.Sum_Var();
}

